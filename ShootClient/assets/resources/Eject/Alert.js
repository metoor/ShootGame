
cc.Class({
    extends: require('UI-PanelWindow'),

    properties: {
        title:cc.Label,
        content : cc.Label,
        btnOk :cc.Node,
        btnCancel :cc.Node
    },
    setOptions(options){
        this.autoClose = true;
        if(options.title != undefined){
            this.title.string = options.title
        }
        if(options.message != undefined){
            this.content.string = options.message
        }
        if(options.autoClose != undefined){
            this.autoClose = options.autoClose
        }
        this.options = options;
        if(!options.cancel){
            this.btnOk.x = 0;
            this.btnCancel.active = false;
        }
    },
    onClickOk(e,d){
        SoundMgr.playButtonEffect()
        if(this.autoClose){
            global.EJMgr.popUI();
        }
        this.options.ok && this.options.ok();
    },
    onClickCancel(e,d){
        SoundMgr.playButtonEffect()
        if(this.autoClose){
            global.EJMgr.popUI();
        }
        this.options.cancel && this.options.cancel();
    }
});
