
var Animate = cc.Class({
    name : 'animates',
    properties:{
        aname : "",
        list:[cc.SpriteFrame],
        delay : 0
    }
})

var Enum = require('Enum')

cc.Class({
    extends: require('BoxCollider'),

    properties: {
        spriteAnimate : require('SpriteAnimate'),
        skillLauncher : require('SkillLauncher'),
        animates :[Animate],
        isLocal : {
            type : Boolean,
            default : false,
            visible:false
        },
        _status : -2
    },
    onLoad(){
        this._direction = cc.v2(0,0);
        this.setStatus(Enum.RoleState.Idle);
        this.config = global.DataCfg['player']
        this.skillLauncher.camp = this.camp;
        this.shootType = 0;
        this.shootAngle = ''
        // this.pNet = this.node.getComponent('PNetComponent')
    },

    start(){
        this.activeColldier = true;
    },

    setOptions(user){//设置用户数据
        this.user = user;
    },

    setData(d){ //设置游戏数据
        d.st = d.st || 0
        d.sa == d.sa || 'right'

        this.setShootType(d.st);
        this.setShootAngle(d.sa);
    },

    getAnimate(name){
        return this.animates.find(item=>{return item.aname == name})
    },

    playAnimate(name,count,cb){
        var animate = this.getAnimate(name)
        this.spriteAnimate.reset();
        for(var i=0; i<animate.list.length; i++){
            this.spriteAnimate.addFrame(animate.list[i]);
        }
        this.spriteAnimate.setDelayTime(animate.delay);
        this.spriteAnimate.startAnimate(count || 1,cb);
    },

    setStatus(s){
        if(this._status != s){
            this._status = s;
            this.playAnimate('u',-1,function(){})
        }
    },

    setDirection(vec2){
        // cc.log('setDirection ',this.user.id,vec2);
        this._direction.x = vec2.x
        this._direction.y = vec2.y;
    },
    setShootType(t){
        if(this.shootType == t){
            return;
        }
        this.shootType = t;
        this.startFire();
    },

    setShootAngle(d){
        if(this.shootAngle == d){
            return;
        }
        this.shootAngle = d;
        this.startFire();
        if(this.isLocal){
            global.GameLoop.GameMgr.control.setShootBtn(d);
        }
    },

    startFire(){
        if(this.shootType == 0 && this.isLocal){
            this.skillLauncher.reset();
            this.skillLauncher.isLocal = this.isLocal;
            if(this.shootAngle == 'right'){
                this.skillLauncher.addLauncher(this.config.weapon.normal.right)
            } else {
                this.skillLauncher.addLauncher(this.config.weapon.normal.left)
            }
        }
    },

    setPosition(x,y){
        this.node.x = x;
        this.node.y = y
    },

    getFrameInfo(){

    },

    gameUpdate(dt){
        this.skillLauncher.gameUpdate(dt);
        if(this._direction.equals(cc.Vec2.ZERO)){
            return;
        }
        var x = this._direction.x * dt * 150;
        var y = this._direction.y * dt * 150;
        // if(!this.isLocal){
        //     this.node.x += x
        //     this.node.y += y;
        //     return;
        // }
        var no = this.node
        var start = no.convertToWorldSpace(cc.v2(no.width*no.anchorX+x,no.height*no.anchorY+y))
        var bulletRect = cc.rect(start.x-cc.winSize.width/2-no.width*no.anchorX,start.y-cc.winSize.height/2-no.width*no.anchorY,no.width,no.height)
        var node = global.GameLoop.GameMgr.colliderMgr.checkMove(bulletRect)
        if(!node){
            this.node.x += x
            this.node.y += y;
            // cc.log('移动:',global.Data.id,x,y)
        } else {
            this._direction = cc.Vec2.ZERO;
            // global.GameLoop.GameMgr.send(this._direction)
        }
    }
});
