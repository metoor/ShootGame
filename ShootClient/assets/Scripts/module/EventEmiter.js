const Events =require('events');
const eventEmiter=new Events();
eventEmiter.off=eventEmiter.removeAllListeners;
module.exports=eventEmiter;