// Learn cc.Class:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/life-cycle-callbacks.html

cc.Class({
    name : 'wy-PlatformBase',
    properties: {
    },
    init(){
        this.os = 'web'
        this.isWechat = 0
        this.isBaidu = 0;
        this.isQQPlay = 0
        this.videoAd = 0;
    },
    login() {
    },
    share() {
    },
    getLaunchOption() {
    },
    getReferrerInfo() {
    },

    clearOption() {
    },
 
    destroyLoginButton() {
    },
    showLoading(){

    },
    hideLoading() {
    },
    getLocation() {
    },

    startRecord() {
    },
    stopRecord() {
    },

    playRecord() {
    },
    playOneRecord() {
    },
    exit() {
    },
    screenshot() {
    },
    cleanOldAssets() {
    },

    triggerGC() {
    },

    destroyClubBtn() {
    },

    createClubBtn() {
    },

    copyString(str) {
    },

    openSetting(cb) {
    },

    vibrateLong() {
    },
    loadRemoteImg(url,cb){
        cb(null,url)
    },
    showToast(){
        
    },
    createBanner(){
        return null;
    },
    createRewardedVideoAd(){
        return null;
    },
    showVideo(id,cb){
        if (!CC_WECHATGAME || !wx.createRewardedVideoAd) {
            setTimeout(function(){
                cb(true)
            },1000)
            return 
        }
    }
});
