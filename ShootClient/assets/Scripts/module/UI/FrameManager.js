

var FrameManager = {
    children:[],
    add(pathReady){
        this.children.push(pathReady)
    },
    remove(pathReady){
        var chd = this.children.findIndex(item=>{return item.uuid == pathReady.uuid})
        if(chd >-1){
            this.children.splice(chd,1);
        }
    },
    gameUpdate(dt){
        // if(this.runTime)
        for(var i=0; i<this.children.length; i++){
            this.children[i].gameUpdate(dt);
            if(this.children[i].x < -100){
                cc.log('')
            }
        }
    },
    removeAll(){
        this.children.splice(0,this.children.length);
    },
}

module.exports = FrameManager;